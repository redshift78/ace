{-# Language OverloadedStrings #-}

module AppUI
  ( defaultApp
  ) where

import Brick.Main
import Brick.Types
import Brick.Util (on, fg)
import Brick.Widgets.Core
import Brick.Widgets.Border
import Brick.Widgets.Border.Style

import qualified Graphics.Vty as V
import qualified Brick.AttrMap as A
import qualified Graphics.Vty.Attributes as VA
import qualified Graphics.Vty.Attributes.Color as VAC

import UIState

data UIEvent  = PlaceHolderEvent deriving (Eq, Show)
type UINames  = ()

defaultApp :: App UIState UIEvent UINames
defaultApp = App
  { appDraw         = drawUI
  , appChooseCursor = \_ _ -> Nothing
  , appHandleEvent  = eventHandler
  , appStartEvent   = return
  , appAttrMap      = attrMap
  }

borderWithLabel' title        = withBorderStyle unicodeRounded . borderWithLabel title
activeBorderWithLabel title   = overrideAttr borderAttr "activeBorder"    . borderWithLabel' title
inactiveBorderWithLabel title = overrideAttr borderAttr "inactiveBorder"  . borderWithLabel' title

focusableBorderWithLabel :: UIState -> ActiveWindow -> String -> Widget UINames -> Widget UINames
focusableBorderWithLabel state name title content
  | hasFocus    = activeBorderWithLabel   activeTitle   content
  | otherwise   = inactiveBorderWithLabel inactiveTitle content
  where hasFocus      = activeWindow state == name
        activeTitle   = withAttr "activeTitle"    $ str title
        inactiveTitle = withAttr "inactiveTitle"  $ str title

namedColors :: [String]
namedColors = colors <> map ("Bright " <>) colors
  where colors  = [ "Black", "Red", "Green", "Yellow", "Blue", "Magenta", "Cyan", "White" ]

namedColorAttrs :: [A.AttrName]
namedColorAttrs = [ "black", "red", "green", "yellow", "blue", "magenta", "cyan", "white"
                  , "brightBlack", "brightRed", "brightGreen", "brightYellow", "brightBlue"
                  , "brightMagenta", "brightCyan", "brightWhite" ]

greyColorAttrs :: [A.AttrName]
greyColorAttrs = [ A.attrName ("color" <> show (c+216)) | c <- [0..23] ]

extraColorAttrs :: [A.AttrName]
extraColorAttrs = [ A.attrName ("color" <> show i) | i <- [0..215] ]

namedColorOptions :: Widget UINames
namedColorOptions = vBox left <+> translateBy (Location (1,0)) (vBox right)
  where examples      = map (\a -> withAttr a (str "█ ")) namedColorAttrs
        left          = zipWith (<+>) (take 8 examples) $ map str $ take 8 namedColors
        right         = zipWith (<+>) (drop 8 examples) $ map str $ drop 8 namedColors

widgetColorGrid :: (Int, Int) -> [A.AttrName] -> Widget UINames
widgetColorGrid (cols, rows) attrs = vBox [ row n | n <- [0..rows-1] ]
  where row n = hBox $ map (\a -> withAttr a (str "█")) $ take cols $ drop (n*cols) attrs

{-
widgetForegroundColors :: UIState -> Widget UINames
widgetForegroundColors state = focusableBorderWithLabel state ForegroundColorWindow "Foreground" namedColorOptions
-}

widgetAllColorsGrid :: Widget UINames
widgetAllColorsGrid = namedColors <=> greys <=> extra
  where namedColors = widgetColorGrid (16,1) namedColorAttrs
        greys       = widgetColorGrid (24,1) greyColorAttrs
        extra       = widgetColorGrid (36,6) extraColorAttrs

widgetForegroundColors :: UIState -> Widget UINames
widgetForegroundColors state = focusableBorderWithLabel state ForegroundColorWindow "Foreground" widgetAllColorsGrid

widgetBackgroundColors :: UIState -> Widget UINames
widgetBackgroundColors state = focusableBorderWithLabel state BackgroundColorWindow "Background" namedColorOptions

widgetColor240 :: UIState -> Widget UINames
widgetColor240 state = focusableBorderWithLabel state Color240Window "Color 240" content
  where cols    = 36  :: Int
        rows    = 6   :: Int
        row n   = hBox [ withAttr (A.attrName ("color" <> show (c+n*cols))) (str "█") | c <- [0..cols-1] ]
        greys   = hBox [ withAttr (A.attrName ("color" <> show (c+6*cols))) (str "█") | c <- [0..23] ]
        content = vBox [ row n | n <- [0..rows-1] ] <=> translateBy (Location (0,1)) greys

drawUI :: UIState -> [ Widget UINames ]
drawUI state = [ fcs <+> bcs <+> widgetColor240 state ]
  where fcs = widgetForegroundColors state
        bcs = widgetBackgroundColors state

eventHandler :: UIState -> BrickEvent UINames UIEvent -> EventM UINames (Next UIState)
eventHandler state (VtyEvent e)
  | e == V.EvKey V.KEsc         []  = halt state
  | e == V.EvKey (V.KChar '\t') []  = continue $ state { activeWindow = nextW }
  | e == V.EvKey (V.KChar 'q')  []  = halt state
  | otherwise                       = continue state
  where window  = activeWindow state
        nextW   | window == maxBound  = minBound
                | otherwise           = succ window

eventHandler state _  = continue state

attrMap :: UIState -> A.AttrMap
attrMap state = A.attrMap V.defAttr $
  [ ("text",          V.white         `on` V.black)
  , ("black",         V.black         `on` V.black)
  , ("red",           V.red           `on` V.black)
  , ("green",         V.green         `on` V.black)
  , ("yellow",        V.yellow        `on` V.black)
  , ("blue",          V.blue          `on` V.black)
  , ("magenta",       V.magenta       `on` V.black)
  , ("cyan",          V.cyan          `on` V.black)
  , ("white",         V.white         `on` V.black)
  , ("brightBlack",   V.brightBlack   `on` V.black)
  , ("brightRed",     V.brightRed     `on` V.black)
  , ("brightGreen",   V.brightGreen   `on` V.black)
  , ("brightYellow",  V.brightYellow  `on` V.black)
  , ("brightBlue",    V.brightBlue    `on` V.black)
  , ("brightMagenta", V.brightMagenta `on` V.black)
  , ("brightCyan",    V.brightCyan    `on` V.black)
  , ("brightWhite",   V.brightWhite   `on` V.black)
  , ("activeTitle",   V.brightWhite   `on` V.black  `VA.withStyle` VA.bold)
  , ("inactiveTitle", V.white         `on` V.black  `VA.withStyle` VA.dim)
  , ("activeBorder",  V.brightWhite   `on` V.black  `VA.withStyle` VA.bold)
  , ("inactiveBorder",V.white         `on` V.black  `VA.withStyle` VA.dim)
  ] <> [ (A.attrName ("color" <> show i), VAC.Color240 i `on` V.black) | i <- [0..239] ]
