{-# Language OverloadedStrings #-}

module UIState
  ( UIState (..)
  , ActiveWindow (..)
  , defaultUIState
  ) where

data ActiveWindow = ForegroundColorWindow
                  | BackgroundColorWindow
                  | Color240Window
                  deriving (Eq, Show, Ord, Bounded, Enum)

newtype UIState = UIState
  { activeWindow  :: ActiveWindow
  }

defaultUIState :: UIState
defaultUIState = UIState
  { activeWindow  = minBound
  }


