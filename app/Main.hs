module Main where

import AppUI
import UIState

import Brick.Main

main :: IO ()
main = do
  defaultMain defaultApp defaultUIState
  return ()
